from evalys.jobset import JobSet
from evalys.visu import *
import sys
import matplotlib.pyplot as plt

import os
import matplotlib
import matplotlib.patches as mpatch
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import random
import evalys

def plot_gantt(jobset, ax=None, title="Gantt chart",
               labels=True, palette=None, alpha=0.4,
               time_scale=False,
               color_function=None,
               label_function=None):
    # Palette generation if needed
    if color_function is None:
        def color_randrobin_select(job, palette):
            return palette[job.unique_number % len(palette)]
        color_function = color_randrobin_select

    if label_function is None:
        def job_id_label(job):
            return job['jobID']
        label_function = job_id_label

    # Get current axe to plot
    if ax is None:
        ax = plt.gca()

    df = jobset.df.copy()
    labeled_jobs, unique_numbers = evalys.visu.map_unique_numbers(df)
    df["unique_number"] = unique_numbers

    if time_scale:
        df['submission_time'] = pd.to_datetime(df['submission_time'], unit='s')
        df['starting_time'] = pd.to_datetime(df['starting_time'], unit='s')
        df['execution_time'] = pd.to_timedelta(df['execution_time'], unit='s')

    def plot_job(job):
        col = color_function(job, palette)
        duration = job['execution_time']
        for itv in job['allocated_processors'].intervals():
            (y0, y1) = itv
            x0 = job['starting_time']
            if time_scale:
                # Convert date to matplotlib float representation
                x0 = matplotlib.dates.date2num(x0.to_pydatetime())
                finish_time = matplotlib.dates.date2num(
                    job['starting_time'] + job['execution_time']
                )
                duration = finish_time - x0
            rect = mpatch.Rectangle((x0, y0), duration,
                                    y1 - y0 + 0.9,
                                    facecolor=col,
                                    edgecolor='black',
                                    linewidth=0.5)
            if labels:
                if job.name in labeled_jobs:
                    annotate(ax, rect, str(label_function(job)))
            ax.add_artist(rect)

    # apply for all jobs
    df.apply(plot_job, axis=1)

    # set graph limits, grid and title
    ax.set_xlim(df['submission_time'].min(), (
        df['starting_time'] + df['execution_time']).max())
    ax.set_ylim(jobset.res_bounds[0]-1, jobset.res_bounds[1]+2)
    ax.grid(True)
    ax.set_title(title)

plt.rcParams["figure.figsize"] = (100,100)
js = JobSet.from_csv(sys.argv[1])

# js.df["stretch"] = js.df["stretch"] / js.df["stretch"].max()

cm = plt.cm.get_cmap('Purples')
cm_rej = plt.cm.get_cmap('Blues')

norm = matplotlib.colors.LogNorm(vmin=js.df["stretch"].min(), vmax=js.df["stretch"].max())
# norm = matplotlib.colors.Normalize(vmin=js.df["stretch"].min(), vmax=js.df["stretch"].max())
# norm = matplotlib.colors.PowerNorm(gamma=0.1, vmin=js.df["stretch"].min(), vmax=js.df["stretch"].max())

print(js.df["stretch"].max())
def color(job, palette):
  if "rej" in job.workload_name:
    return cm_rej(norm(job.stretch))
  return cm(norm(job.stretch))


time_scale=False
fig, axe = plt.subplots(nrows=2, sharex=True, figsize=(12, 8))

evalys.visu.plot_job_details(js.df, js.MaxProcs, ax=axe[1],
                          time_scale=time_scale)

plot_gantt(js,ax=axe[0], color_function=color, time_scale=time_scale)

plt.savefig(os.path.basename(sys.argv[1])+".pdf")
plt.clf()

