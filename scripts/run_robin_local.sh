#!/usr/bin/env bash

# we keep the base path of the exp as this script moves into a res folder"
BASE_PATH=".."

DATA=".."/${1}

RESULTS="${2}"

OUTPUTCSV="results"

cd $RESULTS

while IFS=',' read -ra VALS; do
# Id
ID=${VALS[0]}
# Socket
SOCKET=${VALS[1]}
# Algo
ALGO=${VALS[2]}
# Plateform
PLATFORM=${VALS[3]}
# Workload
WORKLOAD=${VALS[4]}
# allocation
ALLOCATION=${VALS[5]}
# threshold
THRESHOLD=${VALS[6]}
# conflicts_resolution_policy
CPOLICY=${VALS[7]}
# threshold policy
TPOLICY=${VALS[8]}
# filter
TFPOLICY=${VALS[9]}

export RUST_LOG=polys=info

if [ "$ALGO" == "fcfs" ]; then
cat > $ID.rob <<- EOF
batcmd: >
  batsim -s tcp://localhost:${SOCKET} -p ${DATA}/$PLATFORM -w \
  ${DATA}/workloads/$WORKLOAD -e $ID/$ID --disable-schedule-tracing --disable-machine-state-tracing \
  -m master_host0 --config-file ../inputs/conf.json
schedcmd: polys --port $SOCKET
output-dir: "$ID"
simulation-timeout: 3000
ready-timeout: 5
success-timeout: 1000
failure-timeout: 10
EOF

elif [ "$ALGO" == "easy" ]; then
cat > $ID.rob <<- EOF
batcmd: >
  batsim -q -s tcp://localhost:${SOCKET} -p ${DATA}/$PLATFORM -w \
  ${DATA}/workloads/$WORKLOAD -e $ID/$ID --disable-schedule-tracing --disable-machine-state-tracing \
  -m master_host0 --config-file ../inputs/conf.json
schedcmd: polys -e --port $SOCKET
output-dir: "$ID"
simulation-timeout: 3000
ready-timeout: 5
success-timeout: 1000
failure-timeout: 10
EOF

elif [ "$ALGO" == "redirection" ]; then
cat > $ID.rob <<- EOF
batcmd: >
  batsim -q -s tcp://localhost:${SOCKET} -p ${DATA}/$PLATFORM -w \
  ${DATA}/workloads/$WORKLOAD -e $ID/$ID --disable-schedule-tracing --disable-machine-state-tracing \
  -m master_host0 --config-file ../inputs/conf.json
schedcmd: polys redirection --port $SOCKET --threshold $THRESHOLD --redirection-allocation $ALLOCATION --conflict-policy ${CPOLICY} --threshold-policy ${TPOLICY} --threshold-filter-policy ${TFPOLICY}
output-dir: "$ID"
simulation-timeout: 3000
ready-timeout: 5
success-timeout: 1000
failure-timeout: 10
EOF

elif [ "$ALGO" == "redirection_bf" ]; then
cat > $ID.rob <<- EOF
batcmd: >
  batsim -q -s tcp://localhost:${SOCKET} -p ${DATA}/$PLATFORM -w \
  ${DATA}/workloads/$WORKLOAD -e $ID/$ID --disable-schedule-tracing --disable-machine-state-tracing \
  -m master_host0 --config-file ../inputs/conf.json
schedcmd: polys redirection -eb --port $SOCKET --threshold $THRESHOLD --redirection-allocation $ALLOCATION --conflict-policy ${CPOLICY} --threshold-policy ${TPOLICY} --threshold-filter-policy ${TFPOLICY}
output-dir: "$ID"
simulation-timeout: 3000
ready-timeout: 5
success-timeout: 1000
failure-timeout: 10
EOF
fi

robin $ID.rob --json-logs 2> ${ID}_robin.out 1>&2
ROBSTATUS=$?

# Move the file, so we can rewrite it with tail
mv ${ID}/${ID}_jobs.csv ${ID}/${ID}_jobs.csv.tmp

Rscript ${BASE_PATH}/scripts/compute_stretches.R ${ID}/${ID}_jobs.csv.tmp ${ALGO} ${ID}/${ID}_jobs.csv.with_stretch.tmp ${ID}/metrics.csv

# We print raw simulation output
# sed "s/^/job:${ID},/" ${ID}/${ID}_jobs.csv.with_stretch.tmp | tail -n +2 >> ${ID}/${ID}_jobs.csv

# I think, that maybe we don't need to gather all jobs anymore.
# cat ${ID}/${ID}_jobs.csv

# We gather the metrics of the instance
sed '2q;d' ${ID}/metrics.csv | sed "s/^/robid:${ID},return:${ROBSTATUS},${ID},${ALGO},/" >> $OUTPUTCSV.$ALGO.csv

done < "/dev/stdin"

