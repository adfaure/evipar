#!/usr/bin/env bash
while IFS=',' read -ra VALS; do

echo $VALS >> robin_log.out

# Id
ID=${VALS[0]}
# Socket
SOCKET=${VALS[1]}
# Algo
ALGO=${VALS[2]}
# Plateform
PLATFORM=${VALS[3]}
# Workload
WORKLOAD=${VALS[4]}
# allocation
ALLOCATION=${VALS[5]}
# threshold
THRESHOLD=${VALS[6]}
# conflicts_resolution_policy
CPOLICY=${VALS[7]}
# threshold policy
TPOLICY=${VALS[8]}
# filter
TFPOLICY=${VALS[9]}

#export RUST_LOG=polys

# timeouts
simulto="86400"
successto="86400"
readyto="3000"

if [ "$ALGO" == "fcfs" ]; then
cat > $ID.rob <<- EOF
batcmd: >
  batsim -q --disable-schedule-tracing --disable-machine-state-tracing  -s tcp://localhost:${SOCKET} -p $PLATFORM -w $WORKLOAD -e $ID/$ID -m master_host0 --config-file conf.json
schedcmd: polys --port $SOCKET
output-dir: "$ID"
simulation-timeout: ${simulto}
ready-timeout: ${readyto}
success-timeout: ${successto}
failure-timeout: 10
EOF

elif [ "$ALGO" == "easy" ]; then
cat > $ID.rob <<- EOF
batcmd: >
  batsim -q --disable-schedule-tracing --disable-machine-state-tracing  -s tcp://localhost:${SOCKET} -p $PLATFORM -w $WORKLOAD -e $ID/$ID -m master_host0 --config-file conf.json
schedcmd: polys -e --port $SOCKET
output-dir: "$ID"
simulation-timeout: ${simulto}
ready-timeout: ${readyto}
success-timeout: ${successto}
failure-timeout: 10
EOF

elif [ "$ALGO" == "redirection" ]; then
cat > $ID.rob <<- EOF
batcmd: >
  batsim -q --disable-schedule-tracing --disable-machine-state-tracing -s tcp://localhost:${SOCKET} -p $PLATFORM -w $WORKLOAD -e $ID/$ID -m master_host0 --config-file conf.json
schedcmd: polys redirection --port $SOCKET --threshold $THRESHOLD --redirection-allocation $ALLOCATION --conflict-policy ${CPOLICY} --threshold-policy ${TPOLICY} --threshold-filter-policy ${TFPOLICY}
output-dir: "$ID"
simulation-timeout: ${simulto}
ready-timeout: ${readyto}
success-timeout: ${successto}
failure-timeout: 10
EOF

elif [ "$ALGO" == "redirection_bf" ]; then
cat > $ID.rob <<- EOF
batcmd: >
  batsim -q --disable-schedule-tracing --disable-machine-state-tracing -s tcp://localhost:${SOCKET} -p $PLATFORM -w $WORKLOAD -e $ID/$ID -m master_host0 --config-file conf.json
schedcmd: polys redirection -eb --port $SOCKET --threshold $THRESHOLD --redirection-allocation $ALLOCATION --conflict-policy ${CPOLICY} --threshold-policy ${TPOLICY} --threshold-filter-policy ${TFPOLICY}
output-dir: "$ID"
simulation-timeout: ${simulto}
ready-timeout: ${readyto}
success-timeout: ${successto}
failure-timeout: 10
EOF
fi

robin $ID.rob --json-logs 2> ${ID}_robin.out 1>&2
ROBSTATUS=$?

# Move the file, so we can rewrite it with tail
mv ${ID}/${ID}_jobs.csv ${ID}/${ID}_jobs.csv.tmp

Rscript ./compute_stretches.R ${ID}/${ID}_jobs.csv.tmp ${ALGO} \
${ID}/${ID}_jobs.csv.with_stretch.tmp ${ID}/metrics.csv >> compute_stretches.stderr 2>&1

# We print raw simulation output
# sed "s/^/job:${ID},/" ${ID}/${ID}_jobs.csv.with_stretch.tmp | tail -n +2 >> ${ID}/${ID}_jobs.csv

# I think, that maybe we don't need to gather all jobs anymore.
# cat ${ID}/${ID}_jobs.csv

# We gather the metrics of the instance
# sed '2q;d' ${ID}/metrics.csv | sed "s/^/robid:${ID},return:${ROBSTATUS},${ID},${ALGO},/"
sed '2q;d' ${ID}/metrics.csv | sed "s/^/robid:${ID},return:${ROBSTATUS},${ID},${ALGO},/" > ${ID}/forspark.txt 2> ${ID}/sed.stderr

cat ${ID}/forspark.txt

done < "/dev/stdin"
