{
  kapack ? import (
    fetchTarball
    "https://github.com/oar-team/kapack/archive/master.tar.gz") {}
}:

with kapack;
let
  # installedPkgs = import <nixpkgs> {};
  rDeps = with pkgs.rPackages; [
      xtable
      knitr
      rmarkdown
      tidyverse
      viridis
      ggrepel
    ];

  MyRStudio = pkgs.rstudioWrapper.override {
    packages = rDeps;
  };


  MyRScripts = pkgs.rWrapper.override {
    packages = rDeps;
  };

in
pkgs.mkShell {
  buildInputs = [
    MyRStudio
    pkgs.glibcLocales
    ##
    (python.withPackages (ps: with ps; with pythonPackages; [
      docopt
      jupyter
      ipython
      kapack.evalys4
      aiofiles
      sortedcontainers
    ]))
  ];
}
