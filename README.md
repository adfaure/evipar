# Evipar

## Organization
The repository is organized as follows:

**/Notebooks/** # Contains all R script to generated the visualisations.

**/data_loss/** # Contains results from old experiments

**/experiments/** # Contains the results of the published experiments.

**/images/** # Contains the images of the experimentations appearing into the paper (and more).

**/job_spark/** # The job spark used to run the experiments.

**/scripts/** # A bunch of script related to the experimentations runs.

**/evalys.nix** # A nix file usefull to enter into the environment with all the tools needed to generate the visalisations.


## Nix

To install all dependencies to generate the figure: 

```bash
nix-shell evalys.nix
```

## Scheduler

The scheduler source code can be found [here](https://gitlab.inria.fr/adfaure/polysched).

The project uses the version 1.40 of the simulator Batsim [available here](https://gitlab.inria.fr/batsim/batsim/tree/v1.4.0).